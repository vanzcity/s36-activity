const express = require("express")
const router = express.Router()
const TaskController = require("../controllers/TaskController")

router.get("/", (request, response) => {
	TaskController.getAll().then(result => response.send(result))
})


router.post("/new-task", (request, response) => {
	TaskController.createTask(request.body).then(result => response.send(result))
})

router.put("/:id", (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then(result =>response.send(result))
})

router.get("/:id", (request, response) => {
	TaskController.getOne(request.params.id).then(result => response.send(result))
})

module.exports = router 
